package com.tsystems.javaschool.tasks.pyramid;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        int rowNumber = countCols(inputNumbers.size());
        int[][] result = new int[rowNumber][rowNumber * 2 - 1];
        return fillMatrix(result, new LinkedList<>(inputNumbers.stream().sorted().toList()));
    }

    private int[][] fillMatrix(int[][] matrix, Queue<Integer> queue) {
        for (int i = 0; i < matrix.length; i++) {
            int zeroCount = matrix[0].length / 2 - i;
            for (int l = 0; l < zeroCount; l++) {
                matrix[i][l] = 0;
                matrix[i][matrix[0].length - l - 1] = 0;
            }
            boolean isNumber = true;
            for (int j = zeroCount; j < matrix[0].length - zeroCount; j++) {
                if (isNumber) {
                    matrix[i][j] = queue.poll();
                    isNumber = false;
                } else {
                    matrix[i][j] = 0;
                    isNumber = true;
                }
            }
        }
        return matrix;
    }

    private int countCols(int lstSize) {
        int i = 1;
        while (lstSize > 0) {
            lstSize -= i;
            i++;
        }
        if (lstSize != 0) {
            throw new CannotBuildPyramidException();
        }
        return i - 1;
    }
}
