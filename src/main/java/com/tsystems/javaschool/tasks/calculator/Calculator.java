package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Calculator {

    private int pos = 0;
    private List<String> tokens;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        this.tokens = getTokens(statement);
        if (tokens == null) {
            return null;
        }
        Double res = add();
        return new DecimalFormat("#.####").format(res).toString().replaceAll(",", ".");
    }

    private Double add() {
        Double frst = mult();
        while (pos < tokens.size()) {
            String sign = tokens.get(pos);
            if (!sign.equals("+") && (!sign.equals("-"))) {
                break;
            } else {
                pos++;
            }
            Double scnd = mult();
            if (sign.equals("+")) {
                frst += scnd;
            } else {
                frst -= scnd;
            }
        }
        return frst;
    }

    private Double mult() {
        Double frst = group();
        while (pos < tokens.size()) {
            String sign = tokens.get(pos);
            if (!sign.equals("*") && (!sign.equals("/"))) {
                break;
            } else {
                pos++;
            }
            Double scnd = group();
            if (sign.equals("*")) {
                frst *= scnd;
            } else {
                frst /= scnd;
            }
        }
        return frst;
    }

    private Double group() {
        String curr = tokens.get(pos);
        Double res;
        if (curr.equals("(")) {
            pos++;
            res = add();
            if (pos < tokens.size()) {
                pos++;
                return res;
            }
        }
        pos++;
        return Double.parseDouble(curr);
    }

    private List<String> getTokens(String statement) {
        if (statement == null || statement.equals("")) {
            return null;
        }
        List<String> tokens = new ArrayList<>();
        final String signs = "+-*/";
        int openBr = 0;
        int closeBr = 0;
        statement = statement.trim().replaceAll(" ", "");
        while (statement.length() > 0) {
            if ('.' == statement.charAt(0)) {
                return null;
            }
            if (Character.isDigit(statement.charAt(0))) {
                int i = 0;
                String tmp = "";
                boolean dotFlag = true;
                while (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.') {
                    if (statement.charAt(i) == '.') {
                        if (i == statement.length() - 1) {
                            return null;
                        }
                        if (dotFlag && Character.isDigit(statement.charAt(i + 1))) {
                            dotFlag = false;
                        } else return null;
                    }
                    tmp += statement.charAt(i);
                    i++;
                    if (statement.length() == i) {
                        tokens.add(tmp);
                        return checkBrackets(tokens, openBr, closeBr);
                    }
                }
                tokens.add(tmp);
                statement = statement.substring(i);
            }
            if (signs.indexOf(statement.charAt(0)) != -1) {
                if (tokens.size() == 0) {
                    return null;
                }
                if (signs.contains(tokens.get(tokens.size() - 1))) {
                    return null;
                } else {
                    tokens.add(String.valueOf(statement.charAt(0)));
                    if (statement.length() > 1) {
                        statement = statement.substring(1);
                    }
                }
            }
            if (statement.charAt(0) == '(') {
                openBr += 1;
                tokens.add(String.valueOf(statement.charAt(0)));
                if (statement.length() > 1) {
                    statement = statement.substring(1);
                }
            }
            if (statement.charAt(0) == ')') {
                if (openBr == 0) {
                    return null;
                }
                closeBr += 1;
                tokens.add(String.valueOf(statement.charAt(0)));
                if (statement.length() > 1) {
                    statement = statement.substring(1);
                }
            }
            if ((!Character.isDigit(statement.charAt(0))) &&
                    (signs.indexOf(statement.charAt(0)) == -1) &&
                    ((statement.charAt(0) != ')') || (statement.charAt(0) != '('))) {
                return null;
            }
        }
        return checkBrackets(tokens, openBr, closeBr);
    }

    private List<String> checkBrackets(List<String> tokens, int openBr, int closeBr) {
        if (openBr != closeBr) {
            return null;
        } else return tokens;
    }
}
